import numpy as np
import pandas as pd
from functools import partial
import torch
from torcheval.metrics import TopKMultilabelAccuracy

def topkmulticlass(y_true,y_pred,k=None,criteria="exact_match"):
    if k is None:
        m=TopKMultilabelAccuracy(k=y_pred.shape[1]-1,criteria=criteria)
    else:
        m=TopKMultilabelAccuracy(k=k,criteria=criteria)
    m.update(torch.from_numpy(y_pred),torch.from_numpy(y_true))
    return m.compute().item()


def custom_confusion_matrices(y_true, y_pred, labels, only_false=False, score=False,topk=None, criteria="exact_match"):
    n_classes = y_pred.shape[1]
    y_pred_bin =  y_pred >= 1/y_pred.shape[1]
    
    label_leak = []
    scores=[]
    for i in range(n_classes):
        idx_true_i = y_true[:,i] == 1
        n_class_i = idx_true_i.sum()
        idx_false_classif_i = (y_pred_bin[idx_true_i] == y_true[idx_true_i]).sum(1) < n_classes

        if only_false:
            # Indexes (of the truly class i samples), with mismatch between the true and predicted
            data_to_sum=y_pred_bin[idx_true_i][idx_false_classif_i]
            #print("class ",i,", #=",idx_false_classif_i.shape[0],"n_false_classif=",idx_false_classif_i.sum())
        else:
            data_to_sum=y_pred_bin[idx_true_i]
        
        if score:
            if topk is None:
                scores.append((n_class_i-idx_false_classif_i.sum())/n_class_i)
            else:
                scores.append(topkmulticlass(y_true[idx_true_i], data_to_sum.astype(float),k=topk,criteria=criteria))
            
        label_leak.append(data_to_sum.sum(0).tolist())

    label_leak = np.array(label_leak)#*((1-np.eye(label_leak.shape[1])) if 
    
    if only_false:
        label_leak=label_leak
    out_idx = ["True={}".format(i) for i in labels]
    out = pd.DataFrame(data=label_leak.astype(int),
                        columns=["Pred={}".format(i) for i in labels],
                        index=out_idx)
    if score:
        if (topk is None):
            score_name = "Accuracy (%)"
        else:
            score_name = "Top-{} (%)".format(topk)
            
        out=pd.concat([out,pd.DataFrame(data=scores,index=out_idx,columns=[score_name])],axis=1)
    return out


def check_exp(df, ref_count=20):
    all_cols = [s for s in list(df) if (s.startswith("data_opts__") or s.startswith("model_opts__"))] + ["match"]
    nunique = [df[c].unique() for c in all_cols]

    fixed_cols = [(c, "{}: {}".format(c, i[0])) for i, c in zip(nunique, all_cols) if i.shape[0] == 1]

    varying_cols = [(c, "{}: {}".format(c, i)) for i, c in zip(nunique, all_cols) if i.shape[0] > 1]

    d = {c: i.tolist() for i, c in zip(nunique, all_cols)}
    fmt_input = lambda x: "\"{}\"".format(x) if any(s.isalpha() for s in str(x)) else str(x)

    d_def = "d={" + ",\n".join(["\"{}\":[{}]".format(k, ", ".join(list(map(fmt_input, v)))) for k, v in d.items()]) + "\n}"

    sout = "\n---Fixed conditions\n"
    sout += "\n".join([f[1] for f in fixed_cols])

    sout += "\n\n---Varying conditions\n"
    sout += "\n".join([f[1] for f in varying_cols])

    thedf = df.drop(columns=[f[0] for f in fixed_cols] + ["idx"])

    dfcount = thedf.groupby([f[0] for f in varying_cols]).apply(lambda dd: dd.count())["test__AUROC"]
    sout += "\n\n---Runs count != {}\n".format(ref_count)
    sout += dfcount[dfcount != ref_count].to_string()

    return sout, d_def



def get_med_str(dd,digits=3)-> list:
    """Compute medians for each of the scores(i.e. columns) in the input `pd.DataFrame`."""
    return [str(x) for x in np.round(dd.median(), digits).values]


def get_q_str(dd,digits=3,short=False)->list:
    """Format the inter-quartile range for each score."""
    tmp_quartile = tuple(np.round(dd.quantile([0.25, 0.75]).values, digits))
    Q1, Q3 = tuple(tmp_quartile)
    if short:
        q_str = ["({})".format(round(q3-q1, digits)) for q1, q3 in zip(Q1, Q3)]
    else:
        q_str = ["({}-{})".format(q1, q3) for q1, q3 in zip(Q1, Q3)]
    return q_str


def join_str(*args):
    """Iteratively join elements of lists of `str`.
     The lists must all be the same size."""
    return [[" ".join([a, b]) for a, b in zip(*args)]]


def stats_score(dd, grp_var=None, quartile=True, digits=3, short=False) -> pd.DataFrame:
    """Compute 'med (q1-q3)' strings for each column of the input `pd.DataFrame`."""
    score_var = [s for s in list(dd) if not (s in grp_var)]

    q_str = get_q_str(dd[score_var], digits=digits, short=short)
    med_str = get_med_str(dd[score_var], digits=digits)

    if quartile:

        out = join_str(med_str, q_str)
    else:
        out = med_str

    out = pd.DataFrame(data=out, columns=score_var)
    return out


def nice_format(df, ndigits=2, short=True):
    med = df.median(0).round(ndigits).astype(str)
    q3 = df.quantile(0.75)
    q1 = df.quantile(0.25)
    
    if short:
        iqr = (q3 - q1).round(ndigits).astype(str)
        out = med + " (" + iqr + ")"
    else: 
        out = med + " (" + q1.round(ndigits).astype(str) +" - "+q3.round(ndigits).astype(str)+ ")"
    return out

def format_table(df: pd.DataFrame, grp_var: list, quartile=True, digits=3, gain=None, short=False):
    """Format a table to a good looking presentable format.

    Inputs:

    - df: `pd.DataFrame` containing numerical values to summarize and opts to groupby
    - grp_var: `list` of `str` containing the columns to use to group and summarize the scores.
                The scores are considered to all the left out columns
    - quartile: bool, write the IQR in parenthesis
    Notes: the scores are considered to be all the columns of `df` not listed in `grp_var`.
    """
    f_stats_score = partial(stats_score, grp_var=grp_var, quartile=quartile, digits=digits, short=short)
    dsummary = df.groupby(grp_var).apply(f_stats_score)
    dsummary.index = dsummary.index.droplevel(level=len(grp_var))
    return dsummary


def get_stats(dd, short=False, digits=2):
    l = [dd["test__AUROC"].median(), dd["test__AUROC"].quantile(.25), dd["test__AUROC"].quantile(.75)]
    if short:
        l += ["{} ({})".format(round(l[0], digits), round(l[2] - l[1], digits))]
    else:
        l += ["{} ({}-{})".format(round(l[0], digits), round(l[1], digits), round(l[2], digits))]
    return pd.DataFrame(data=[[l[0], l[-1]]], columns=["med", "desc"])


def get_best_row(dd, colname="med"):
    ibest = dd[colname].argmax()
    return dd.iloc[ibest]


def get_best_ref_table(df, row_name, best_name):
    """Problem specific. """
    if len(best_name) == 1:
        idx = (df[best_name[0]] == "25 min")
        dlvl = 2
    elif len(best_name) == 2:
        idx = (df[best_name[0]] == "25 min") & (df[best_name[1]] == "Ts=1")
        dlvl = 3

    fstats = partial(get_stats, digits=2, short=True)
    dfwlen_med = df.groupby(best_name + [row_name]).apply(fstats).droplevel(dlvl).reset_index()

    sref = ["ref {}".format(k) for k in best_name]
    sbest = ["best {}".format(k) for k in best_name]

    best_rename = {**{"med": "best_med", "desc": "best_desc"}, **{k: v for k, v in zip(best_name, sbest)}}
    ref_rename = {**{"med": "ref_med", "desc": "ref_desc"}, **{k: v for k, v in zip(best_name, sref)}}

    f_get_best_row=partial(get_best_row, colname="med")

    dfbestwlen = dfwlen_med.groupby(row_name).apply(f_get_best_row).rename(columns=best_rename)
    # dfbestwlen.columns = s+["Model name", "best_med","best_q1","best_q3","best_desc"]
    # print(dfbestwlen)

    dfrefwlen_med = df[idx].groupby(best_name + [row_name]).apply(fstats).droplevel(dlvl).reset_index()

    # print(dfrefwlen_med)

    dfrefwlen = dfrefwlen_med.groupby(row_name).apply(f_get_best_row).rename(columns=ref_rename)
    # dfrefwlen.columns = s+["Model name", "ref_med","ref_q1","ref_q3","ref_desc"]

    dfout = pd.concat([dfbestwlen, dfrefwlen], axis=1)
    # print(dfout)
    dfout["Gain"] = (100 * (dfout["best_med"] - dfout["ref_med"]) / dfout["ref_med"]).round(2).astype(str) + "%"
    dfout = dfout[["ref_desc", "best_desc"] + sref + sbest + ["Gain"]]
    dfout.columns = ["Ref. AUROC", "Best AUROC"] + sref + sbest + ["Gain"]

    return dfout[["Ref. AUROC"] + sref + ["Best AUROC"] + sbest + ["Gain"]]


def rename_df(df,renaming=None):
    df.rename(columns=renaming,inplace=True)
    df.index.names=[renaming[s] if s in renaming.keys() else s for s in df.index.names]
    df.columns.names=[renaming[s] if s in renaming.keys() else s for s in df.columns.names]
    return df


def test_custom_confusion_matrix():
    y_true_ = np.array([[0,1,1],
                    [1,0,0],
                    [0,0,1]])

    y_pred_ = np.array([[0,1,1],
                        [1,0,0],
                        [1,1,0]])

    out=custom_confusion_matrices(y_true_, y_pred_, train_targets, 
                            only_false=False, score=True, topk=2, criteria="contain")
    print(out)