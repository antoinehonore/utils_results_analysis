import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.font_manager as font_manager

def plot_roc(ax, All_rocs_tuples, title_str:str, fontdict={'fontsize': 10}, opts=[], alpha=0.2, colors=None, legend=False, pr=False, markers=None, legend_xoffset=0, lw=3, verbose=False,linestyles=None):
    """
    Plot several ROC lines on the same figure and returns the figure object.

    Inputs:

    - All_rocs_tuples: `list` of `tuple`, defined in `patdb_tbox.plots.utils.get_roc_curve`
    - title_str: str, title string
    - fontdict: dict, font information for xy axis and tick labels
    - opts: `list` of str, line names for the legend box, must have the same length as `All_rocs_tuples`
    - alpha: float, opacity of std dev area fill, disable with `alpha`=0
    - colors: `list` of colors in hex format, must have the same length as `All_rocs_tuples`
    """

    # activate latex text rendering
    #rc('text', usetex=True)
    #rc('axes', linewidth=2)
    #rc('font', weight='bold')
    #rcParams['text.latex.preamble'] = [r'\usepackage{sfmath} \boldmath']
    font = {'family': 'sans-serif',
            #'sans-serif': 'DejaVu Sans',
            #'weight': 'bold',
            'size': fontdict["fontsize"]}

    matplotlib.rc('font', **font)

    if colors is None:
        colors = []
        cmap = plt.get_cmap("tab10")(np.linspace(0,1,len(All_rocs_tuples)))
        for c in cmap:
            colors.append(matplotlib.colors.rgb2hex(c))
    
    if not pr:
        ax.plot([0, 1], [0, 1], linestyle='--', lw=lw, color='gray',
            label='Chance', alpha=0.8)
    else:
        for f_score in  np.linspace(0.2, 0.8, num=4):
            x = np.linspace(0.01, 1)
            y = f_score * x / (2 * x - f_score)
            ax.plot(x[y >= 0], y[y >= 0], color="gray", alpha=0.2)
            ax.annotate("f1={0:0.1f}".format(f_score), xy=(0.9, y[45] + 0.02), fontsize=8, color="gray")


    if linestyles is None:
        linestyles = ["-"]*len(All_rocs_tuples)
    if markers is None:
        markers = [None]*len(All_rocs_tuples)


    for iopts, all_rocs_tuples in enumerate(All_rocs_tuples):
        display = [0 for _ in range(len(all_rocs_tuples))]
        n = 100
        mean_fpr = np.linspace(0, 1, n)
        tprs = np.zeros((len(all_rocs_tuples), n))
        aucs = np.zeros(len(all_rocs_tuples))

        for i, (fpr, tpr, thresholds, roc_auc) in enumerate(all_rocs_tuples):
            # display[i] = metrics.RocCurveDisplay(fpr=fpr, tpr=tpr, roc_auc=roc_auc, \
            #      estimator_name='logreg')
            # display[i].plot(ax=ax)

            tprs[i, :] = np.interp(mean_fpr, fpr, tpr)

            aucs[i] = roc_auc

        mean_tpr = np.mean(tprs, axis=0)
        
        mean_tpr[-1] = 1.0
        if pr:
            mean_tpr[-1] = 0.0
        # mean_auc = metrics.auc(mean_fpr, mean_tpr)
        if verbose:
            print(aucs)
        mean_auc = np.median(aucs)
        std_auc = np.std(aucs)
        thescorestr = " ".join(#["AUC="]
                               #+ get_med_str(pd.DataFrame(data=aucs), digits=2)
                               #+ get_q_str(pd.DataFrame(data=aucs),digits=2))
                               [opts[iopts]]
                               )
        ax.plot(mean_fpr, mean_tpr, '-',  # color='royalblue',
                label=thescorestr, #r'AUC = %0.2f $\pm$ %0.2f, %s' % (mean_auc, std_auc, opts[iopts]),
                lw=lw, alpha=.8, color=colors[iopts], linestyle=linestyles[iopts], marker=markers[iopts])

        std_tpr = np.std(tprs, axis=0)
        tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
        tprs_lower = np.maximum(mean_tpr - std_tpr, 0)

        if alpha > 0:
            ax.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=alpha,
                            label=r'$\pm$ 1 std. dev.')
        
        imax = np.argmax((1 - mean_fpr) * mean_tpr)

        if pr:
            imax = np.argmax((mean_fpr * mean_tpr)/(mean_fpr + mean_tpr))

        lopts = {"dashes": [6, 2],
                 "linewidth": 1}

        ax.plot([mean_fpr[imax]] * 2, [0, mean_tpr[imax]], 'k.', **lopts)
        ax.plot([0, mean_fpr[imax]], [mean_tpr[imax]] * 2, 'k.', **lopts)

    font_man = font_manager.FontProperties(**font)#weight='bold', size=fontdict["fontsize"])
    # ax.legend()
    # Change to location of the legend.
    leg = ax.legend(loc="lower right",frameon=True)#, prop=font)
    bb = leg.get_bbox_to_anchor().transformed(ax.transAxes.inverted())
    #transformed(transform.inverted())
    #transformed(transform.inverted())
    xOffset = legend_xoffset
    #yOffset = legend_yoffset

    bb.x0 += xOffset
    bb.x1 += xOffset
    #bb. += yOffset
    #bb.x1 += yOffset
    leg.set_bbox_to_anchor(bb, transform=ax.transAxes)

    ax.set_xlabel("1 - Specificity", fontdict=font)
    ax.set_ylabel("Sensitivity",fontdict=font)
    if pr:
        ax.set_xlabel("Recall", fontdict=font)
        ax.set_ylabel("Precision",fontdict=font)

    ax.set_title(title_str, fontdict=font)

    ax.spines['right'].set_visible(False)
    # ax.spines['left'].set_visible(False)
    ax.spines['top'].set_visible(False)
    # ax.spines['bottom'].set_visible(False)
    #,fontdict=font_man)

    if not legend:
        ax.get_legend().set_visible(False)
    ax.set_xlim([0, 1])
    ax.set_ylim([0, 1])
    theticks=[0,0.2,0.4,0.6,0.8,1];#ax.get_xticks()
    ax.set_xticks(theticks[1:] if 0 in theticks else theticks)
    ax.set_yticks(theticks)
    return ax
